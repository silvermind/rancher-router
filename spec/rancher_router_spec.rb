require_relative '../app/rancher_router'

describe RancherRouter do
  describe "nginx config" do
    before do
      @router = RancherRouter.new(domain: 'rancher.test.com', email: 'test@example.com', service_conf_path: 'tmp/rancher.conf')
    end

    it "should generate nginx config for rancher service" do
      expect(@router.send(:service_conf)).to eq(File.read("spec/data/services.conf"))
    end
  end

end
