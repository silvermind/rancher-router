# Simple Router

Nginx based router to expose rancher through https on given port.
Automatic letsencrypt certification.

## Run
 
Run rancher and then run ones 

    docker rm -f rancher-router && \
      docker pull registry.gitlab.com/backbot/rancher-router:1.0 && \
      docker run --name rancher-router \
      -e RR_DOMAIN=rancher.example.com \
      -e RR_EMAIL=admin@example.com \
      -e RR_NGINX_PORT=9999 \
      -v /var/lib/rancher/etc/ssl:/ssl \
      -v /data/letsencrypt:/etc/letsencrypt \
      -p 80:80 registry.gitlab.com/backbot/rancher-router:1.0
      
    docker logs -f rancher-router

After cert is created stop rancher-router and start again on port 8080

    docker rm -f rancher-router && \
      docker pull registry.gitlab.com/backbot/rancher-router:1.0 && \
      docker run --name rancher-router \
      -e RR_DOMAIN=rancher.example.com \
      -e RR_EMAIL=admin@example.com \
      -e RR_NGINX_PORT=8080 \
      -v /var/lib/rancher/etc/ssl:/ssl \
      -v /data/letsencrypt:/etc/letsencrypt \
      -p 8080:8080 registry.gitlab.com/backbot/rancher-router:1.0

### Environment Variables

* `RR_URL`
Url pointing to rancher instance.
* `RR_PORT`
Port of rancher instance. Default: 8080

* `RR_DOMAIN`
Domain pointing to rancher instance.
* `RR_NGINX_PORT`
Port where nginx will listen on. Default: 8080

* `RR_EMAIL`
Email for letsencrypt.

**Hint: Rancher must be startet with '--name RR_URL' and router with '--link RR_URL'.**

