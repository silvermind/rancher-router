#!/bin/bash

set -e

log=/var/log/forego.log
/router/bin/forego start -f /router/Procfile > $log 2>&1 &
bundle exec ruby app/init.rb
tail -f $log

