require 'erb'
class RenderContext

  def initialize(router)
    @router = router
  end

  def render(template)
    ERB.new(template, nil, '-').result(binding)
  end

end