FROM nginx:1.9.12
MAINTAINER Georg Kunz & Daniel Schlegel, CloudGear <contact@cloudgear.net>

### cron and whenever ###
RUN DEBIAN_FRONTEND=noninteractive apt-get -qq update \
    && apt-get -qqy install --no-install-recommends apt-utils wget
RUN wget https://nginx.org/keys/nginx_signing.key -O - | apt-key add -

RUN DEBIAN_FRONTEND=noninteractive apt-get -qq update \
    && apt-get -qqy upgrade \
    && apt-get -qqy install --no-install-recommends cron rsyslog
### cron and whenever ###

### install ruby ###
RUN apt-get -qq update \
    && apt-get -qqy upgrade \
	&& apt-get -qqy install -y --no-install-recommends \
		build-essential \
		patch \
		git-core \
		git \
		wget \
		vim \
		bzip2 \
		ca-certificates \
		curl \
		libffi-dev \
		libgdbm3 \
		libssl-dev \
		libyaml-dev \
		procps \
		zlib1g-dev \
		liblzma-dev \
		libgmp-dev \
		ruby-dev


RUN git clone https://github.com/letsencrypt/letsencrypt

ENV RUBY_MAJOR 2.3
ENV RUBY_VERSION 2.3.0
ENV RUBY_DOWNLOAD_SHA256 ba5ba60e5f1aa21b4ef8e9bf35b9ddb57286cb546aac4b5a28c71f459467e507
ENV RUBYGEMS_VERSION 2.6.2

# skip installing gem documentation
RUN echo 'install: --no-document\nupdate: --no-document' >> "$HOME/.gemrc"

# some of ruby's build scripts are written in ruby
# we purge this later to make sure our final image uses what we just built
RUN buildDeps=' \
		autoconf \
		bison \
		gcc \
		libbz2-dev \
		libgdbm-dev \
		libglib2.0-dev \
		libncurses-dev \
		libreadline-dev \
		libxml2-dev \
		libxslt-dev \
		make \
		ruby \
		python-dev \
		python3 \
		telnet \
		python-colorama-whl \
		python-chardet-whl \
		libaugeas0 \
		dialog \
		augeas-lenses \
		python-distlib-whl python-html5lib-whl python-pip-whl python-pkg-resources \
		python-requests-whl python-setuptools-whl python-six-whl python-urllib3-whl \
        python-virtualenv python3-virtualenv virtualenv \
	' \
	&& set -x \
	&& apt-get install -y --no-install-recommends $buildDeps \
	&& rm -rf /var/lib/apt/lists/* \
	&& mkdir -p /usr/src/ruby \
	&& curl -fSL -o ruby.tar.gz "http://cache.ruby-lang.org/pub/ruby/$RUBY_MAJOR/ruby-$RUBY_VERSION.tar.gz" \
	&& echo "$RUBY_DOWNLOAD_SHA256 *ruby.tar.gz" | sha256sum -c - \
	&& tar -xzf ruby.tar.gz -C /usr/src/ruby --strip-components=1 \
	&& rm ruby.tar.gz \
	&& cd /usr/src/ruby \
	&& autoconf \
	&& ./configure --disable-install-doc \
	&& make -j"$(nproc)" \
	&& make install \
	&& gem update --system $RUBYGEMS_VERSION \
	&& rm -r /usr/src/ruby


ENV HOME /router
WORKDIR $HOME
# install things globally, for great justice
ENV GEM_HOME /usr/local/bundle

ENV BUNDLE_GEMFILE=$HOME/Gemfile \
    BUNDLE_JOBS=2 \
    BUNDLE_PATH=$GEM_HOME
# don't create ".bundle" in all our apps
ENV BUNDLE_APP_CONFIG $GEM_HOME
ENV PATH $GEM_HOME/bin:$PATH

RUN gem install bundler \
	&& bundle config --global path "$GEM_HOME" \
	&& bundle config --global bin "$GEM_HOME/bin" \
	&& bundle config --global frozen 1 # throw errors if Gemfile has been modified since Gemfile.lock

COPY Gemfile      $HOME/Gemfile
COPY Gemfile.lock $HOME/Gemfile.lock

RUN bundle install --path $BUNDLE_PATH --deployment #--without test development

RUN apt-get purge -y --auto-remove $buildDeps

### end install ruby ###

#RUN cd /usr/local && git clone https://github.com/letsencrypt/letsencrypt
RUN mkdir -p /etc/nginx/services && \
    rm -rf /etc/nginx/conf.d
RUN mkdir -p /tmp/letsencrypt/.well-known/acme-challenge/

COPY app $HOME/app
COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx.crt /etc/nginx/nginx.crt
COPY nginx.key /etc/nginx/nginx.key
COPY Procfile $HOME/Procfile
COPY bin/ $HOME/bin/
COPY run.sh $HOME/run.sh

CMD ["./run.sh"]
#ENTRYPOINT ["./run.sh"]
#CMD ["/router/bin/forego", "start", "-f", "/router/Procfile"]


