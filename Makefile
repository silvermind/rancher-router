all: build

build:
	docker build --force-rm=true --no-cache=true -t registry.gitlab.com/backbot/rancher-router:1.0 .

push-1.0: # this is the version with tcp enabled.
	docker build -t registry.gitlab.com/backbot/rancher-router:1.0 .
	docker tag  registry.gitlab.com/backbot/rancher-router:1.0 registry.gitlab.com/backbot/rancher-router:1.0
	docker push registry.gitlab.com/backbot/rancher-router:1.0
